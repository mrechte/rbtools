#!/bin/bash

for d in $(find -maxdepth 1 -type d)
do
    if [[ $d != '.' ]]
    then
        the_dir=$(basename $d)
        echo "Processing $the_dir directory..."
        gimp-console -idf --batch-interpreter python-fu-eval \
	        -b "import sys;sys.path=['.']+sys.path;from xcf2png import run;run(\"$the_dir\");pdb.gimp_quit(1)"
	fi
done
