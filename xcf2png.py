#!/bin/env python2

"""
See https://ntcore.com/?p=509
To run from the CLI:
    gimp-console -idf --batch-interpreter python-fu-eval \
        -b "import sys;sys.path=['.']+sys.path;from xcf2png import run;run(<the_directory>);pdb.gimp_quit(1)"
 -i: Run without a user interface
 -d: Do not load patterns, gradients, palettes, or brushes. Often  useful in non-interactive situations where startup time is to be minimized.
 -f: Do  not  load  any fonts. No text functionality will be available if this option is used.
 --batch-interpreter python-fu-eval: specifies the procedure to use to process batch events. The  default is to let Script-Fu evaluate the commands.
 -b: Execute <command> non-interactively. This option may appear multiple times.  The <command> is passed to the batch interpreter.
        When <command> is - the commands are read from standard input.

"""
import os, os.path
from sys import exit

from gimpfu import *


def process_file(file, the_dir, the_sub_dir):
    (base, ext) = os.path.splitext(file)
    tfile = the_sub_dir + os.sep + base + '.png'
    img = pdb.gimp_file_load(the_dir + os.sep + file, file)
    #img.layers[0].opacity = float(level)
    img.merge_visible_layers(NORMAL_MODE)
    # interlace, compression, BKDG, GAMA, OFFs, PHYs, TIME
    pdb.file_png_save(img, img.layers[0], tfile, tfile, 0, 9, 1, 0, 0, 1, 1)
    pdb.gimp_image_delete(img)


def run(the_dir):
    if not os.path.isdir(the_dir):
        print("Missing " + the_dir + " images directory")
        exit(1)
    the_sub_dir = the_dir + os.sep + "png"
    for file in os.listdir(the_dir):
        (base, ext) = os.path.splitext(file)
        if ext == '.xcf':
            # Intentionaly tested here, to not create useless subdirs
            if not os.path.isdir(the_sub_dir):
                    os.mkdir(the_sub_dir)
            # only if target does not already exists    
            if not os.path.exists(the_sub_dir + os.sep + base + '.png'):
                print("Processing file " + file)
                process_file(file, the_dir, the_sub_dir)

