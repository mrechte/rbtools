#!/bin/env python3

# see https://tanyaschlusser.github.io/slides/Python-Fu-in-GIMP.slides.html

import locale, sys, os, math, argparse

from openpyxl import load_workbook

from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4
from reportlab.lib.textsplit import getCharWidths

script_version = '0.4.0'
WB_VERSIONS = (2, ) # compatible workbook versions (Para.VER)

LINW = 1 # line width point
TXTMAR = 0.1*cm # text margin


def string_length(string, font, size):
    t = getCharWidths(string, font, size)
    size = sum(t)
    return size


class RBError(Exception):
    def __init__(self, message, ws=None, row=None, col=None):
        self.message = message
        self.ws = ws
        self.row = row
        self.col = col

    def __str__(self):
        location = ""
        if self.ws:
            location = self.ws.title
            if self.row and self.col:
                location += "[%d:%d]: " % (self.row, self.col)
            else:
                location += ": "
        return location + self.message

class Prm(object):
    pass


class RB(object):

    def __init__(self, args):
        self.args = args

    def find_para(self, val):
        for row in self.ws_para.iter_rows(min_row=1, max_col=3):
            # print(row[1])
            if row[1].value == val:
                ret = row[2].value
                return ret
        raise RBError("parameter %s not found ind Data sheet" % val)


    def frame_cell(self, ws):
        c = self.c
        prm = self.prm
        # main frame
        c.setLineWidth(LINW)
        c.rect(0, 0, prm.CELW, prm.CELH)
        # V sep 1
        c.line(prm.VSEP1, 0, prm.VSEP1, prm.HSEP)
        # V sep 2
        c.line(prm.VSEP2, 0, prm.VSEP2, prm.CELH)
        # V sep 3
        if prm.VSEP3:
            c.line(prm.VSEP3, 0, prm.VSEP3, prm.CELH)
        # H sep
        c.line(0, prm.HSEP, prm.VSEP2, prm.HSEP)
    

    def sum_cell(self, ws, tot_t, tot_r, tot_unspec):
        c = self.c
        prm = self.prm
        self.frame_cell(ws)
        # total in km partial area
        x = prm.VSEP2/2
        y = (prm.HSEP + (prm.CELH-prm.HSEP)/2) - prm.NBR1SIZ/2
        c.setFont("Helvetica-Bold", prm.NBR1SIZ)
        c.drawCentredString(x, y, "TOTAL")
        # Values in drawing area
        x = prm.VSEP2 + TXTMAR
        if tot_t + tot_r != 0.0:
            # We distinguish T/R totals and ignore unspecified
            tot = tot_t + tot_r
            pct_t = tot_t / tot
            pct_r = tot_r / tot
            s = locale.format_string('T %.3f', tot_t, True) + " ({:.0%})".format(pct_t)
            c.drawString(x, y, s)
            y -= prm.NBR1SIZ
            s = locale.format_string('R %.3f', tot_r, True) + " ({:.0%})".format(pct_r)
            c.drawString(x, y, s)
        else:
            # unspecified
            c.drawString(x, y, locale.format_string('%.3f', tot_unspec, True))


    def one_cell(self, ws, num, tr, km1, tot, comment):
        # print(num, km1, tot, comment)
        c = self.c
        prm = self.prm
        self.frame_cell(ws)
        # Main km
        x = prm.VSEP2/2
        y = (prm.HSEP + (prm.CELH-prm.HSEP)/2) - prm.NBR1SIZ/2
        tpl = tr + ' %.3f' if tr else '%.3f'
        c.setFont("Helvetica-Bold", prm.NBR1SIZ)
        #tpl.format(km1) does not use locale
        c.drawCentredString(x, y, locale.format_string(tpl, km1, True))
        # Cell number
        fnum = '{:05d}'.format(num)
        x = prm.VSEP1/2
        y = prm.HSEP/2 - prm.NBR3SIZ/2
        c.setFont("Helvetica", prm.NBR3SIZ)
        c.drawCentredString(x, y, fnum)
        # Total
        x = prm.VSEP1 + (prm.VSEP2-prm.VSEP1)/2
        y = prm.HSEP/2 - prm.NBR2SIZ/2
        c.setFont("Helvetica", prm.NBR2SIZ)
        c.drawCentredString(x, y, locale.format_string('%.3f', tot, True))
        # Image
        fimage = ws.title.lower() + os.sep + 'png' + os.sep + fnum + '.png'
        if not os.path.exists(fimage):
            print("warning: %s cell has no image" % fnum)
        else: 
            x = prm.VSEP2 + 1
            y = 1
            w = prm.DRAW - 2*LINW
            h = prm.CELH - 2*LINW
            c.drawInlineImage(fimage, x, y, width=w, height = h)
        # Comment
        if prm.VSEP3 and comment:
            x = prm.VSEP3 + TXTMAR
            xmax = x + prm.COMW - 2*TXTMAR
            y = prm.CELH - prm.COMSIZ
            to = c.beginText(x, y)
            to.setFont("Helvetica", prm.COMSIZ)
            lines = comment.split(os.linesep)
            for line in lines:
                words = line.split()
                # print("origin x", to.getX())
                for word in words:
                    w = string_length(word, "Helvetica", prm.COMSIZ)
                    if to.getX() + w > xmax:
                        # line overflows => implicit line break
                        # print(to.getX(), w)
                        to.moveCursor(0, prm.COMSIZ)
                    to.textOut(word+' ')
                # explicite line break
                to.moveCursor(0, prm.COMSIZ)
            c.drawText(to)


    def myPage(self, ws, pagen, pages):
        c = self.c
        prm = self.prm
        # left header
        x = prm.LEFMAR
        # image or text ?
        for ext in ('png', 'gif', 'jpeg'):
            fimage = 'rb.' + ext
            if os.path.exists(fimage):
                break
            fimage = None
        if fimage:
            y = prm.PAGH - prm.TOPMAR + TXTMAR
            c.drawInlineImage(fimage, x, y)
        else:
            y = prm.PAGH - prm.HEAMAR - prm.HEASIZ
            c.setFont("Helvetica", prm.HEASIZ)
            c.drawString(x, y, prm.HEAL)
        # center header
        s = prm.HEAC
        if ws and ws.title != "Data":
            s += " ({})".format(ws.title)
        x += (prm.PAGW - prm.RIGMAR - prm.LEFMAR) / 2
        y = prm.PAGH - prm.HEAMAR - prm.HEASIZ
        c.setFont("Helvetica-Bold", prm.HEASIZ)
        c.drawCentredString(x, y, s)
        # right header (made a bit smaller)
        HEA3SIZ = round(prm.HEASIZ *0.7)
        c.setFont("Helvetica", HEA3SIZ)
        s = '{0} - page {1:d}/{2:d}'.format(prm.HEAR, pagen, pages)
        x = prm.PAGW - prm.RIGMAR - string_length(s, "Helvetica", HEA3SIZ)
        c.drawString(x, y, s)
        # translate to ease drawing
        c.translate(prm.LEFMAR, prm.BOTMAR)


    def cell_start(self, ws, page, line, col, pages):            
        c = self.c
        prm = self.prm
        if prm.GENORD == 2: # Horizontal, then vertical
            if col > prm.COLPAG:
                line += 1
                col = 1
            if line > prm.LIGPAG:
                page += 1
                line = col = 1
                c.showPage() # Produce page, reset canvas
                self.myPage(ws, page, pages)
        else: # Vertical, then horizontal
            if line > prm.LIGPAG:
                col += 1
                line = 1
            if col > prm.COLPAG:
                page += 1
                line = col = 1
                c.showPage() # Produce page, reset canvas
                self.myPage(ws, page, pages)
        # offset canvas to cell pos
        c.saveState()
        c.translate((col - 1)*prm.CELW, (prm.LIGPAG - line)*prm.CELH)
        return page, line, col


    def cell_end(self, ws, line, col):
        c = self.c
        prm = self.prm
        c.restoreState()
        # next cell
        if prm.GENORD == 2:
            col += 1
        else:
            line += 1
        return line, col


    def genSurveyRB(self):
        c = self.c
        prm = self.prm
        # how many pages we will get ?
        cell_per_page = prm.COLPAG * prm.LIGPAG
        pages = self.args.pages
        # first page
        self.myPage(None, 1, pages)
        # Iter through worksheet lines (skip first line, values only)
        page = line = col = 1
        for cell in range(0, pages*cell_per_page):

            page, line, col = self.cell_start(None, page, line, col, pages)

            # draw cell
            self.frame_cell(None)

            line, col = self.cell_end(None, line, col)

        # flush last page      
        c.showPage() # TODO potential blank page ?


    def genRB(self, ws):
        c = self.c
        prm = self.prm
        # how many pages we will get ?
        cell_per_page = prm.COLPAG * prm.LIGPAG
        pages = math.ceil((ws.max_row - 1 + 1) / cell_per_page) # +1 for distance totals
        # first page
        self.myPage(ws, 1, pages)
        # Iter through worksheet lines (skip first line, values only)
        page = line = col = ws_row = 1
        tot = tot_t = tot_r = tot_unspec = 0.0
        for row in ws.iter_rows(min_row=2, max_col=4, values_only=True):
            # next ws row (for error reporting)
            ws_row += 1
            page, line, col = self.cell_start(ws, page, line, col, pages)

            # check values OK
            if type(row[0]) != int:
                raise RBError("Invalid numeric data for cell number", ws, ws_row, 1)
            if type(row[1]) not in (int, float):
                raise RBError("Invalid numeric data for distance", ws, ws_row, 2)
            tr = None
            if row[2]:
                tr = row[2].upper()
                if tr not in ('T', 'R'):
                    raise RBError("Invalid T/R value", ws, ws_row, 3)
            # accumulate
            if tr == 'T':
                # total track
                tot_t += row[1]
            elif tr == 'R':
                # total road
                tot_r += row[1]
            else:
                # total unspecified
                tot_unspec += row[1]
            # draw cell
            tot += row[1]
            self.one_cell(ws, row[0], tr, row[1], tot, row[3])

            line, col = self.cell_end(ws, line, col)

        # distance total
        if tot_t + tot_r != 0.0 and tot_unspec != 0.0:
            print("Warning: there are unspecified T/R distances")
        page, line, col = self.cell_start(ws, page, line, col, pages)
        self.sum_cell(ws, tot_t, tot_r, tot_unspec)
        line, col = self.cell_end(ws, line, col)

        # flush last page      
        c.showPage() # TODO potential blank page ?


    def myRB(self):
        prm = self.prm
        # Create the canvas
        if prm.PAGORI == 1:
            width, height = A4 # portrait
        else:
            height, width = A4 # landscape
        # Computed extra PRM from other values (all measures in point unit 1/72")
        prm.PAGW = width
        prm.PAGH = height
        prm.CELW = (width - prm.LEFMAR - prm.RIGMAR) / prm.COLPAG # Cell Width
        prm.CELH = (height - prm.TOPMAR - prm.BOTMAR) / prm.LIGPAG # Cell Height
        if prm.BOXH1 + prm.BOXH2 != 100:
            raise RBError("BOXH1 + BOXH2 <> 100")
        prm.HSEP = prm.CELH * prm.BOXH2 / 100 # Horizontal separator between km and index / tot
        if prm.BOXW1 + prm.BOXW2 + prm.BOXW3 + prm.BOXW4 != 100:
            raise RBError("BOXW1 + BOXW2 + BOXW3 + BOXW4 <> 100")
        prm.VSEP1 = prm.CELW * prm.BOXW1 / 100 # Vertical separator between index box and total box
        prm.VSEP2 = prm.CELW * (prm.BOXW1 + prm.BOXW2)/ 100 # Vertical separator drawing
        prm.VSEP3 = prm.CELW * (prm.BOXW1 + prm.BOXW2 + prm.BOXW3)/ 100 if prm.BOXW4 else None # Vertical separator comment
        prm.DRAW = prm.VSEP3 - prm.VSEP2 if prm.VSEP3 else prm.CELW - prm.VSEP2 # drawing width
        prm.COMW = prm.CELW - prm.VSEP3 if prm.BOXW4 else None # comment width
        #print(prm.CELW, prm.COMW); exit()
        rb_name = "rb_survey.pdf" if self.args.survey else "rb.pdf"
        c = canvas.Canvas(rb_name, pagesize=(width, height))
        self.c = c
        if self.args.survey:
            # Generate a surevy road book
            self.genSurveyRB()
        else:
            # Generate main road book
            self.genRB(self.ws_data)
            # Generate variable parts if any
            for ws in self.ws_var:
                self.genRB(ws)
        # Actually generates the PDF to disk
        c.save()


    def run(self):
        locale.setlocale(locale.LC_ALL, '')
        self.wb = load_workbook('rb.xlsx', read_only=True)
        # Make sure we have Data and Para sheets
        self.ws_para = self.ws_data = None
        self.ws_var = list()
        for sheet in self.wb:
            if sheet.title == 'Para':
                self.ws_para = sheet
            elif sheet.title == 'Data':
                self.ws_data = sheet
            elif sheet.title != 'Main':
                self.ws_var.append(sheet) 
        if not (self.ws_data and self.ws_data):
            raise RBError("You must have a Data and Para sheets in your workbook")

        # Get parameters
        prm = Prm()
        # Spécifiques			
        prm.HEAC = self.find_para('HEAC') # Titre du road book (en-tête centre)
        prm.HEAL = self.find_para('HEAL') # Organisation (en-tête gauche)
        prm.HEAR = self.find_para('HEAR') # Date (en-tête droite)
			        
        # Taille des cases Taille des cases (hauteurs et largeurs en pourcentage)
        prm.BOXH1 = self.find_para('BOXH1')	# Hauteur des 2 premières lignes de la case (km partiel)
        prm.BOXH2 = self.find_para('BOXH2')	# Hauteur de la troisième ligne de la case (index et km total)
        prm.BOXW1 = self.find_para('BOXW1')	# Largeur de la première colonne de la case (index de case)
        prm.BOXW2 = self.find_para('BOXW2')	# Largeur de la deuxième colonne de la case (km total)
        prm.BOXW3 = self.find_para('BOXW3')	# Largeur de la troisième colonne de la case (dessin)
        prm.BOXW4 = self.find_para('BOXW4')	# Largeur de la quatrième colonne "commentaires" (0 = pas de colonne commentaires)
			        
        # Formattage des cases			
        prm.NBR1FMT = self.find_para('NBR1FMT')	# Format nombre kilométrage partiel
        prm.NBR1SIZ = self.find_para('NBR1SIZ')	# Taille nombre kilométrage partiel (points)
        prm.NBR2FMT = self.find_para('NBR2FMT')	# Format nombre kilométrage total
        prm.NBR2SIZ = self.find_para('NBR2SIZ')	# Taille nombre kilométrage total (points)
        prm.NBR3SIZ = self.find_para('NBR3SIZ')	# Taille nombre index case (points)
        prm.COMSIZ = self.find_para('COMSIZ')	# Taille du texte commentaire (points)
			        
        # Impression			
        prm.COLPAG = self.find_para('COLPAG') # Nombre de colonnes par page
        prm.LIGPAG = self.find_para('LIGPAG') # Nombre de lignes par page
        prm.GENORD = self.find_para('GENORD') # Ordre de génération des cases: 1 = Vertical, puis horizontal, 2 = Horizontal, puis vertical
        prm.HORGEN = self.find_para('HORGEN') # Génération horizontale: 1 = Gauche vers droite, 2 = Droite vers gauche
        prm.VERGEN = self.find_para('VERGEN') # Génération verticale: 1 = Haut vers bas, 2 = Bas vers haut
        prm.PAGORI = self.find_para('PAGORI') # Orientation de la page (1 = Portrait, 2 = Paysage)
        prm.LEFMAR = self.find_para('LEFMAR')*cm # Marge gauche (cm)
        prm.RIGMAR = self.find_para('RIGMAR')*cm # Marge droite (cm)
        prm.TOPMAR = self.find_para('TOPMAR')*cm # Marge haut (cm)
        prm.BOTMAR = self.find_para('BOTMAR')*cm # Marge bas (cm)
        prm.HEAMAR = self.find_para('HEAMAR')*cm # Marge en-tête (cm)
        prm.FOOMAR = self.find_para('FOOMAR')*cm # Marge bas de page (cm)
        prm.HEASIZ = self.find_para('HEASIZ') # Taille en-tête (points)
        # Internal
        prm.VER = self.find_para('VER') # Version (DO NOT CHANGE)

        if prm.VER not in WB_VERSIONS:
            raise RBError("Your workbook is from a version (%d) incompatible with this program" % prm.VER)
                    
        self.prm = prm

        self.myRB()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='rbtools')
    parser.add_argument('--version', action='version', version=script_version)
    parser.add_argument("-s", "--survey", help="generate a survey road book", action="store_true")
    parser.add_argument("-p", "--pages", help="number of pages for survey road book (default 6)", default=6, type=int)
    args = parser.parse_args()
    
    rb = RB(args)
    try:
        rb.run()
    except RBError as e:
        print(e)
        exit(1)

