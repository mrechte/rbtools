# Welcome to rbtools resurrected !

Rbtools is a set of tools to produce nicely formatted Road Books as PDF files.

This project was initiated in year 2003 as an MS Excel 97 VBA workbook and was made public as a freeware on http://www.randodetente.org (dead site).

At that time, onboard GPS and tracking systems were less common than nowadays, but being a nostalgic of paper Road Books and recently faced with the making of a new road book for my 4WD club members, I had to react. 

Considering using MS products was out of scope, and that Libre Office never produced a feature rich programming language, I decided to start again from scratch.

This new version is based on the following free and open source tools:
- Libre Office: for the data entry of the road book, just the textual part (cell number, distance, observation)
- Gimp: for the drawing part
- Python: for the programming aspect
- Bash: for the automation

On top of that, I recommend a digitizing tablet like a WACOM Intuos S (60 € on Amazon) to ease the drawing (functional under Linux, except the 4 buttons I could not make work).

Although all this tools are available under Windows, I have not (and have no plan to) tested. This project focuses on the Linux platform.

The legacy directory contains the initial rbtools (VBA) project.

# Installation

## Pre-requisite

### Arch Linux

Libre Office, Gimp and Python3 are usually already installed. If not:

    # pacman -S libreoffice-fresh python gimp

Python Imaging Library (PIL), Report lab, openpyxl:

    # pacman -S python-pillow python-reportlab python-openpyxl


Python2 for Gimp batch interface:

    # pacman -S python2


### Other distros

TODO

## Create a new road book

1. Create a dedicated directory and copy these files:

- rb.xlsx
- rb.py
- xcf2png.py
- xcf2png.sh

> you will need a bash terminal and cd to the created directory.

2. Make sure rb.py and xcf2png.sh are executable:

        $ chmod +x rb.py xcf2png.sh

3. Create a subirectoy "data" to place your Gimp drawings:

        $ mkdir data

4. Open rb.xlsx and enter your parameters in the Para worksheet

5. Create a survey road book:

        $ ./rb.py --survey --pages=10

6. Print the rb_survey.pdf
 
7. Do your survey with the printed empty road book
 
8. Input your data into the Data worksheet, we recommend numbering cells with a step of 10
 
9. Make sure there are no other worksheets except Main, Para and Data (other worksheets would be considered as variants)
 
10. With Gimp, draw each cell, saved in the data sub-directory (same name as the Data worksheet but in lower case),
 the file name has to be exactly the cell number formatted on 5 digits (eg. cell 10 gives 00010.xcf).
 
11. Generate the png files:
 
        $ ./xcf2png.sh

12. Generate the road book:
 
        $ ./rb.py

13. Print the rb.pdf
